BLUE='\034'
NC='\030'

echo ""
echo "--- Linking binaries against libraries with hardcoded paths ---" 
echo ""
echo "Description:Test different recipes for building and linking libraries and binaries."
echo "According to the arguments set for compilation and link, the library dependency can be differs. The link can be either managed like a symbolic link, or a hard link pointing to a fixed location. Using \'ldd\' on the library shows the nature of the link with the binary."
echo ""
echo "Clean binaries" 
rm -f ./bin/myBin*.exe
rm -f ./lib/libmyLib*.so*
rm -f ./lib/myLibSrc.o


echo ""
echo "Build the dynamic library: libmyLib.so" 
tput setaf 4 
echo "# gcc -fPIC -g -c -Wall lib/myLibSrc.C -o lib/myLibSrc.o"
echo "# gcc -shared -o lib/libmyLib.so lib/myLibSrc.o"
tput sgr0 
gcc -fPIC -g -c -Wall lib/myLibSrc.C -o lib/myLibSrc.o
gcc -shared -o lib/libmyLib.so lib/myLibSrc.o


echo ""
echo "Recipe 1: Build with option -L and -l to specify the dynamic library name and location" 
tput setaf 4 
echo "# g++ bin/myBin.C -I . -L ./lib -lmyLib -o bin/myBinOk.exe "
echo "# ldd ./bin/myBinOk.exe | grep myLib"
tput sgr0 
g++ bin/myBin.C -I . -L ./lib -lmyLib -o bin/myBinOk.exe
export LD_LIBRARY_PATH=./lib:$LD_LIBRARY_PATH
tput bold
ldd ./bin/myBinOk.exe  | grep myLib
tput sgr0

echo ""
echo "Recipe 2: Build with dynamic library as an argument :" 
tput setaf 4
echo "# g++ bin/myBin.C -I . -o bin/myBinError.exe ./lib/libmyLib.so"
echo "# ldd ./bin/myBinError.exe | grep myLib"
tput sgr0
g++ bin/myBin.C -I . -o bin/myBinError.exe  ./lib/libmyLib.so 
tput bold
ldd ./bin/myBinError.exe | grep myLib
tput sgr0

echo ""
echo "Recipe 3: Build the dynamic library and define its soname."
echo "          Build the binary with dynamic library as an argument:" 
tput setaf 4
echo "# gcc -shared -Wl,-soname,libmyLib.so.1 -o lib/libmyLibSoName.so.1 lib/myLibSrc.o"
echo "# g++ bin/myBin.C  -I . -o bin/myBinSoName.exe  ../lib/libmyLibSoName.so.1"
echo "# ldd ./bin/myBinSoName.exe | grep myLib"
tput sgr0
gcc -shared -Wl,-soname,libmyLibSoName.so.1 -o lib/libmyLibSoName.so.1 lib/myLibSrc.o
g++ bin/myBin.C  -I . -o bin/myBinSoName.exe  ./lib/libmyLibSoName.so.1 
tput bold
ldd ./bin/myBinSoName.exe | grep myLib
tput sgr0

echo ""




