
### Subject ###
Linking binaries against libraries with hardcoded paths


### Description ###
According to the arguments set for compilation and link, the link nature of the binary with the shared libs can differ. Two link nature have been identified:
 - The binary is linked to the library with a symbolic link
 - The binary is linked to the library with a hardcoded link pointing to a fixed location. 

ldd command on the library shows the nature of the link:
 - binary is linked to the library with a symbolic link:

> ldd ./bin/myBinOk.exe | grep myLib
	libmyLib.so => /home/dev/myDynLibLink/lib/libmyLib.so (0x00007f132a63c000)

 - binary is linked to the library with a hardcoded link pointing to a fixed location: 

> ldd ./bin/myBinError.exe | grep myLib
	/home/dev/myDynLibLink/lib/libmyLib.so (0x00007fcea0db1000)

Hard coded links should be avoided because the hardcoded library path may be incompatible will the runtime environment.


### Analysis ###
To always link the binaries to dynamic libraries using symbolic link, the DT_SONAME has to be set in the dynamic libraries.
Indeed, according to ld manual, setting the DT_SONAME field of the libreary header ("-soname" option for ld) make the linker try to resolve the links using DT_SONAME instead of the filename.  

Specifying the DT_SONAME field of the library will result in:
-  Command line: g++ -o myBin.exe ./lib/libmyDynLib.so
-  Results     : ldd myBin.exe | grep myDynLib
    libmyDynLib.so => /home/dev/testLinkLibs/lib/libmyDynLib.so (0x00007f0151902000) 

Quote from ld manual:
----------------------------------------
"When creating an ELF shared object, set the internal DT_SONAME field to the specified name. When an executable is linked with a shared object which has a DT_SONAME field, then when the executable is run the dynamic linker will attempt to load the shared object specified by the DT_SONAME field rather than the using the file name given to the linker."
---------------------------------------

### Test ###
In the root folder run:
./build.sh

